# Autoinstall

Automatic installation configuration files for various distributions

Look for additinal `README.md` files in each distribution's folder, and further down the directory tree.

The `root` user's password for all example files is `thisisatest`.  Distributions which forbid `root` logins create an `admin` user with the same password.

## License

Licensed under the [EUPL-1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) only.
