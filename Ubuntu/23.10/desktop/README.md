# Ubuntu 23.10 Desktop

## Using the autoinstall files

1.	Select the GRUB menu option "Try or Install Ubuntu"
2.	Press `e` to edit it
3.	After the text `linux    /casper/vmlinuz layerfs-path=minimal.standard.live.squashfs ---`, append the following, pointing to the _directory_ containing the `cloud-init` files:<br />`autoinstall ds='nocloud-net;s=http://server.example/directory/'`
4.	Press `Ctrl-x` or `F10` to boot

## Notes

### Partitioning

- Use capital leters for all partition size suffixes, not what it says in the [`curain` manual](https://curtin.readthedocs.io/en/latest/topics/storage.html#partition-command).  Otherwise the installer will fail. [\[1\]](https://ubuntu.com/server/docs/install/autoinstall-reference)
- The `offset` option is must be specified in bytes, as an integer, with no suffixes.
- You must set `grub_device: true` on the *disk* which contains the [BIOS boot partition](https://en.wikipedia.org/wiki/BIOS_boot_partition).  Otherwise the installer will fail. [\[1\]](https://askubuntu.com/questions/1415360/ubuntu-22-04-autoinstall-storage-section-autoinstall-config-did-not-create-ne)
- You must set `grub_device: true` on the [EFI system partition](https://en.wikipedia.org/wiki/EFI_system_partition) for an UEFI installation.  Otherwise the installer will fail. [\[2\]](https://curtin.readthedocs.io/en/stable/topics/storage.html#partition-command)  This is undocumented.  The mountpoint of this partition must be `/boot/efi`.
