# Ubuntu 20.04 Server

## Using the autoinstall files

1.  Press `ESC` when the boot screen appears
2.  Select your language and press `Enter`
3.  Select the menu option "Install Ubuntu Server" or "Install Ubuntu Server with the HWE kernel"
4.  Press `F6` for "Other Options"
5.  Press `ESC` for to close the menu
6.  Append the following to the kernel command line, pointing to the _directory_ containing the `cloud-init` files:<br />`autoinstall ds=nocloud-net;s=http://server.example/directory/`
7.  Press `Enter` to boot
