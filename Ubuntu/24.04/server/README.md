# Ubuntu 24.04 Server

## Using the autoinstall files

1.	Select the GRUB menu option "Try or Install Ubuntu Server"
2.	Press `e` to edit it
3.	After the text `linux    /casper/vmlinuz ---`, append the following, pointing to the _directory_ containing the `cloud-init` files:<br />`autoinstall ds='nocloud-net;s=http://server.example/directory/'`
4.	Press `Ctrl-x` or `F10` to boot
