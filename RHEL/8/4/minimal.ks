#== Installation Mode (choose one)
text


#== Keyboard, Language, and Time
keyboard --vckeymap=us
lang en_US.UTF-8
timezone America/Chicago --utc --nontp


#== Authentication
rootpw --plaintext thisisatest


#== Installation Source (choose one of cdrom, harddrive, liveimg, nfs, or url)
cdrom


#== Network and Firewall
network --activate --bootproto=dhcp


#== Storage Setup and Bootloader
autopart --type=plain --fstype=ext4
bootloader --location=mbr --boot-drive=sda
clearpart --all --drives=sda --initlabel
zerombr


#== RHEL
eula --agreed
# rhsm --server-hostname=rhsm.test --organization="Org" --activation-key="Key"
%addon com_redhat_kdump --disable
%end


#== Post-install action
reboot


#== Packages
%packages --nocore --excludeWeakdeps

# Core - Mandatory

NetworkManager
# audit
basesystem
bash
coreutils
cronie
curl
dnf
e2fsprogs
filesystem
firewalld
glibc
# grubby
hostname
initscripts
iproute
# iprutils
iputils
# irqbalance
kbd
# kexec-tools
less
man-db
# ncurses
openssh-clients
openssh-server
# parted
passwd
# policycoreutils
procps-ng
rootfiles
rpm
# selinux-policy-targeted
setup
shadow-utils
# sssd-common
# sssd-kcm
subscription-manager
sudo
systemd
# tuned
util-linux
# vim-minimal
xfsprogs
# yum

# Core - Default

# NetworkManager-team
# NetworkManager-tui
# authselect
biosdevname
# dnf-plugins-core
dracut-config-rescue
# iwl100-firmware
# iwl1000-firmware
# iwl105-firmware
# iwl135-firmware
# iwl2000-firmware
# iwl2030-firmware
# iwl3160-firmware
# iwl5000-firmware
# iwl5150-firmware
# iwl6000-firmware
# iwl6000g2a-firmware
# iwl6050-firmware
# iwl7260-firmware
# kernel-tools
libsysfs
linux-firmware
# lshw
# lsscsi
# microcode_ctl
prefixdevname
# sg3_utils
# sg3_utils-libs

# Core - Optional

# dracut-config-generic
# dracut-network
# rdma-core
# selinux-policy-mls
# tboot
%end

%post
dnf -y install http://rhsm.test/pub/katello-ca-consumer-latest.noarch.rpm
subscription-manager register --name=`hostname -f` --org="Org" --activationkey="Key"
%end
