# Red Hat

## Using the autoinstall files

Append the following to the kernel command line, pointing to the Kickstart _file_:

```
inst.ks=http://server.test/directory/file.ks
```
