#== Installation Mode (choose one)
text


#== Keyboard, Language, and Time
keyboard --vckeymap=us
lang en_US.UTF-8
timesource --ntp-disable
timezone America/Chicago --utc


#== Authentication
rootpw --plaintext thisisatest


#== Installation Source (choose one of cdrom, harddrive, liveimg, nfs, or url)
cdrom


#== Network and Firewall
network --activate --bootproto=dhcp


#== Storage Setup and Bootloader
autopart --type=plain --fstype=ext4
bootloader --location=mbr --boot-drive=sda
clearpart --all --drives=sda --initlabel
zerombr


#== RHEL
eula --agreed
# rhsm --server-hostname=rhsm.test --organization="Org" --activation-key="Key"
%addon com_redhat_kdump --disable
%end


#== Post-install action
reboot


#== Packages
%packages --nocore --exclude-weakdeps

# Core - Mandatory

# audit
basesystem
bash
coreutils
cronie
crypto-policies
crypto-policies-scripts
curl
dnf
e2fsprogs
filesystem
firewalld
glibc
# grubby
hostname
iproute
iproute-tc
iputils
# irqbalance
kbd
# kexec-tools
less
logrotate
man-db
# ncurses
openssh-clients
openssh-server
p11-kit
# parted
passwd
# policycoreutils
procps-ng
rootfiles
rpm
rpm-plugin-audit
# selinux-policy-targeted
setup
shadow-utils
# sssd-common
# sssd-kcm
sudo
systemd
util-linux
# vim-minimal
xfsprogs
# yum

# Core - Default

NetworkManager
# NetworkManager-team
# NetworkManager-tui
# authselect
# dnf-plugins-core
dracut-config-rescue
initscripts-rename-device
# iwl100-firmware
# iwl1000-firmware
# iwl105-firmware
# iwl135-firmware
# iwl2000-firmware
# iwl2030-firmware
# iwl3160-firmware
# iwl5000-firmware
# iwl5150-firmware
# iwl6000g2a-firmware
# iwl6050-firmware
# iwl7260-firmware
# kernel-tools
libsysfs
linux-firmware
# lshw
# lsscsi
# microcode_ctl
prefixdevname
# sg3_utils
# sg3_utils-libs

# Core - Optional

# dracut-config-generic
# dracut-network
# rdma-core
# selinux-policy-mls
# tboot

# Core - packagereq

subscription-manager

%end
