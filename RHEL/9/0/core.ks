#== Installation Mode (choose one)
text


#== Keyboard, Language, and Time
keyboard --vckeymap=us
lang en_US.UTF-8
timesource --ntp-disable
timezone America/Chicago --utc


#== Authentication
rootpw --plaintext thisisatest


#== Installation Source (choose one of cdrom, harddrive, liveimg, nfs, or url)
cdrom


#== Network and Firewall
network --activate --bootproto=dhcp


#== Storage Setup and Bootloader
autopart --type=plain --fstype=ext4
bootloader --location=mbr --boot-drive=sda
clearpart --all --drives=sda --initlabel
zerombr


#== RHEL
eula --agreed
# rhsm --server-hostname=rhsm.test --organization="Org" --activation-key="Key"
%addon com_redhat_kdump --disable
%end


#== Post-install action
reboot


#== Packages
%packages --exclude-weakdeps
@Core
%end
