<?xml version='1.0' encoding='UTF-8'?>
<!--
Copyright © 2022 Boian Berberov
Licensed under the EUPL-1.2 only.

License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
SPDX-License-Identifier: EUPL-1.2
-->
<xsl:stylesheet
	xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
	version='1.0'>
	<xsl:output method='xml' indent='yes' encoding='UTF-8'/>

	<xsl:template match="packagereq[@type='mandatory']">
		<mandatory>
			<xsl:apply-templates/>
		</mandatory>
	</xsl:template>

	<xsl:template match="packagereq[@type='default']">
		<default>
			<xsl:apply-templates/>
		</default>
	</xsl:template>

	<xsl:template match="packagereq[@type='optional']">
		<optional>
			<xsl:apply-templates/>
		</optional>
	</xsl:template>

	<xsl:template match="*[@xml:lang]">
		<xsl:apply-templates select='*'/>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select='@*|node()'/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
